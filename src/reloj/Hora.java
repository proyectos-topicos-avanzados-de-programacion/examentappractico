
package reloj;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author 25774
 */
public class Hora extends Thread {

    private final JLabel lbl;

    public Hora(JLabel lbl) {
        this.lbl = lbl;
    }

    @Override
    public void run() {

        try {
            while (true) {
                Date hoy = new Date();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss");
                lbl.setText(df.format(hoy));

                Thread.sleep(1000);
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(Hora.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}